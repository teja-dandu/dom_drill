// Write the code necessary to do the following:

// - Select the section with an id of container without using querySelector.
// - Select the section with an id of container using querySelector.
// - Select all of the list items with a class of "second".
// - Select a list item with a class of third, but only the list item inside of the ol tag.
// - Give the section with an id of container the text "Hello!".
// - Add the class main to the div with a class of footer.
// - Remove the class main on the div with a class of footer.
// - Create a new li element.
// - Give the li the text "four".
// - Append the li to the ul element.
// - Loop over all of the lis inside the ol tag and give them a background color of "green".
// - Remove the div with a class of footer. (edited)







// console.log('hello, world')
//selecting the section using queryId
//without using
// const section1 = document.querySelectorAll('container');
// console.log(section1);

const section = document.getElementById('container');
console.log(section);

// by using querySelector select the section
const section1 = document.querySelector('section');
console.log(section1)



//using query selectorAll list those items and get by the className property
const listItems = document.querySelectorAll('li');
console.log(listItems);



console.log(document.getElementsByClassName('second'));


// first here select using by TagName of orderlist then loop over list and get the values
const listItemsOl = document.getElementsByTagName('ol');
Array.from(listItemsOl).forEach(ol => {
    console.log(ol.getElementsByClassName('third'));
});

//
// creating the new li element and loop throgh first unorderlist and append to the text four as list item
const listItemsUl = document.querySelectorAll('ul');
Array.from(listItemsUl).forEach(ul => {
    console.log(ul);
    const li = document.createElement('li');
    li.textContent = `four`;
    ul.append(li);
});

//
const listItemsOl1 = document.getElementsByTagName('ol');
Array.from(listItemsOl1).forEach(ol1 => {
    // for each element background color to greeenn
    ol1.style.backgroundColor = 'green';
    //this is only for font
    // ol1.style.color = 'green';
    console.log(ol1);
});


// first selecting that by Id and then add text hello, to it

const selection1 = document.getElementById('container');
selection1.innerHTML += "Hello!";
console.log(selection1);


//creating the div class footer add to the classList and then remove the
const div = document.createElement('div');
// div.className = 'footer';
// console.log(div.outerHTML);
div.classList.add('footer');
// adding to the main i.e, to the body
document.body.appendChild(div);
console.log(div.outerHTML);

// console.log(div.outerHTML);
div.classList.remove('footer');
document.body.removeChild(div);
console.log(div.outerHTML);

// adding the main to the class footer
const main = document.querySelector('.footer');
main.classList.add('main');
console.log(main);
// removing the main from the class footer with div container
main.classList.remove('main');
console.log(main);

div.classList.remove('footer');
console.log(div.outerHTML);
// //returns the false value with footer class name there is no div container
console.log(div.classList.contains('footer'));









